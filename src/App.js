import React, { Component } from 'react';
import { Provider } from 'react-redux'
import { ReactReduxFirebaseProvider } from 'react-redux-firebase'
import { createFirestoreInstance } from 'redux-firestore' // <- needed if using firestore
import firebase from 'firebase/app';
import 'firebase/firestore';
import './App.css';
import createReduxStore from './store/createReduxStore';
import RicksResume from './components/RicksResume';
//import PopulateWordCloudDataInFireStore from './components/PopulateWordCloudDataInFireStore';

const firebaseConfig = {
  apiKey: "AIzaSyAuSeF5ibcM56A0y9Na1V53kFK9oP8_d64",
  authDomain: "resume-3e504.firebaseapp.com",
  databaseURL: "https://resume-3e504.firebaseio.com",
  projectId: "resume-3e504",
  storageBucket: "resume-3e504.appspot.com",
  messagingSenderId: "648018751667"
};
firebase.initializeApp(firebaseConfig);
firebase.firestore();

const rrfConfig = {
  userProfile: 'users',
}

const store = createReduxStore()
const rrfProps = {
  firebase,
  config: rrfConfig,
  dispatch: store.dispatch,
  createFirestoreInstance
  // createFirestoreInstance // <- needed if using firestore
}


class App extends Component {

  render() {
    return (
      <Provider store={store}>
      <ReactReduxFirebaseProvider {...rrfProps}>
      <div className="App">
        {/*<PopulateWordCloudDataInFireStore />*/}
        <RicksResume />
      </div>
      </ReactReduxFirebaseProvider>
      </Provider>
    );
  }
}

export default App;
