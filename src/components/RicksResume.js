import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { firestoreConnect } from 'react-redux-firebase'
import WordCloud from 'react-d3-cloud';

class RicksResume extends Component {
  static propTypes = {
    firebase: PropTypes.shape({
      push: PropTypes.func.isRequired
    })
  }
  render() {
    let data = [];
    if (this.props.ricksresume) {
      data = this.props.ricksresume.wordcloud.words;
    }
    const fontSizeMapper = word => Math.log2(word.value) * 5;

    return (
        <WordCloud
           data={data}
           fontSizeMapper={fontSizeMapper}
           rotate={0}
           width={1000}
           height={1000}
        />
    );
  }
}

export default compose(
 firestoreConnect(() => ['ricksresume']), // or { collection: 'todos' }
 connect((state, props) => ({
   ricksresume: state.firestore.data.ricksresume
 }))
)(RicksResume)
