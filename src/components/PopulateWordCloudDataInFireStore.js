import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { withFirestore } from 'react-redux-firebase'

class PopulateWordCloudDataInFireStore extends Component {
  static propTypes = {
    firebase: PropTypes.shape({
      push: PropTypes.func.isRequired
    })
  }

  state = {
    wasSent: false
  }

  testPush = () => {
    const data = [
    { text: 'Rick Garcia', value: 1000000 },
    { text: 'React', value: 1000 },
    { text: 'ReactNative', value: 200 },
    { text: 'AWS', value: 800 },
    { text: 'iOS', value: 1000 },
    { text: 'Android', value: 100 },
    { text: 'AWS', value: 100 },
    { text: 'Kafka', value: 100 },
    { text: 'Kubernetes', value: 1000 },
    { text: 'Docker', value: 100 },
    { text: 'ElasticSearch', value: 100 },
    { text: 'Kibana', value: 100 },
    { text: 'Logstash', value: 100 },
    { text: 'Puppet', value: 100 },
    { text: 'Chef', value: 100 },
    { text: 'Ansible', value: 100 },
    { text: 'Hashicorp Vault', value: 100 },
    { text: 'Python', value: 100 },
    { text: 'Node', value: 100 },
    { text: 'Javascript', value: 100 },
    { text: 'CDN Cloudfront', value: 100 },
    { text: 'Firebase', value: 100 },
    { text: 'Postgress', value: 100 },
    { text: 'MySQL', value: 100 },
    { text: 'GitLab', value: 100 },
    { text: 'GitHub', value: 100 },
    { text: 'GoLang', value: 100 },
    { text: 'Xcode', value: 100 },
    { text: 'Nerd', value: 100 },
    { text: 'Continuous Integration', value: 100 },
    { text: 'automation', value: 100 },
    { text: 'Ubuntu', value: 100 },
    { text: 'RedHat', value: 100 },
    { text: 'Swift', value: 100 },
    { text: 'ObjectiveC', value: 100 },
    { text: 'Security', value: 1000 },
    { text: 'Mongo Webscale j/k', value: 50 },
    ];

    this.props.firestore.collection("ricksresume").doc("wordcloud")
      .set({ words: data })
      .then(() => {
        this.setState({ wasSent: true })
      })
  }

  render() {
    return (
      <div>
        <span>Was sent: {this.state.wasSent}</span>
        <button onClick={this.testPush}>
          Test Push
        </button>
      </div>
    )
  }
}

export default withFirestore(PopulateWordCloudDataInFireStore)
