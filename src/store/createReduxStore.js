import { createStore } from 'redux';
import reducer from '../reducers/reducers';

const initialState = {}

export default () => {
  return createStore(
    reducer,
    initialState,
    // applyMiddleware(...middleware) // to add other middleware
  )
}
